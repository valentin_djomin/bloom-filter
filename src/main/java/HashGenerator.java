import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;

public class HashGenerator {

    private final int numberOfHashes; // optimal number of hash functions
    private final int bitVectorSize; // size of initial bits set
    private final Random random = new Random();

    public HashGenerator(int numberOfHashes, int bitVectorSize) {
        this.numberOfHashes = numberOfHashes;
        this.bitVectorSize = bitVectorSize;
    }

    public Set<Integer> getSetOfHashes(final Object obj) {
        final Set<Integer> setOfHashes = new HashSet<>();
        random.setSeed(obj.hashCode());
        IntStream.range(0, numberOfHashes).forEach(index -> {
            int randomInRange = random.nextInt() % bitVectorSize;
            if (randomInRange < 0) randomInRange = -randomInRange; // should be positive random integer in range 0...sizeOfBitVector
            setOfHashes.add(randomInRange);
        });
        return setOfHashes;
    }
}
