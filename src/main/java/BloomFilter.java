import java.util.BitSet;


public class BloomFilter {

    private final double logOf2 = Math.log(2);

    private final BitSet bitVector; // a vector of bits
    private final HashGenerator hashGenerator; // hash generator to generate hashes

    public BloomFilter(int elementsCount, int bitVectorSize) {
        int k = (int) Math.round(logOf2 * bitVectorSize / elementsCount); // optimal number of hash functions
        if (k <= 0) k = 1; // number of hash functions should be positive value
        this.bitVector = new BitSet(bitVectorSize);
        this.hashGenerator = new HashGenerator(k, bitVectorSize);
    }

    public BloomFilter(int elementsCount) {
        this(elementsCount, BloomFilterConstants.BIT_VECTOR_SIZE_1MB);
    }

    public void add(final Object obj) {
        hashGenerator.getSetOfHashes(obj).forEach(bitVector::set);
    }

    public boolean contains(final Object obj) {
        return hashGenerator.getSetOfHashes(obj).parallelStream().allMatch(bitVector::get);
    }
}
