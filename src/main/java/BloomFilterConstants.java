public class BloomFilterConstants {

    public static final int BIT_VECTOR_SIZE_1MB = 1024 * 1024 * 8;
    public static final int BIT_VECTOR_SIZE_2MB = 2 * BIT_VECTOR_SIZE_1MB;
    public static final int BIT_VECTOR_SIZE_3MB = 3 * BIT_VECTOR_SIZE_1MB;

    public static final String DICTIONARY_DEFAULT_PATH_UNIX = "/usr/share/dict/words";

}
