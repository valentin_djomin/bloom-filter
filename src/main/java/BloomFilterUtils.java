import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BloomFilterUtils {

    public static List<String> readFile(final String fileName) {
        final List<String> strings = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                strings.add(line);
            }
        } catch (FileNotFoundException e) {
            log("File not found. Path: %s", fileName);
        } catch (IOException e) {
            log("Could not read file. Path: %s", fileName);
        }

        return strings;
    }

    public static void log(final String str, final Object... objects) {
        System.out.println(String.format(str, objects));
    }




}

