import java.util.List;
import java.util.Scanner;

public class BloomFilterMain {

    public static void main(String[] args) {
        final String pathToDictionary = BloomFilterConstants.DICTIONARY_DEFAULT_PATH_UNIX;
        BloomFilterUtils.log("Reading dictionary...Default UNIX path: %s", pathToDictionary);
        final List<String> dictionary = BloomFilterUtils.readFile(pathToDictionary);
        BloomFilterUtils.log("Creating Bloom filter...");
        final BloomFilter bloomFilter = new BloomFilter(dictionary.size());
        BloomFilterUtils.log("Inserting dictionary to Bloom filter...");
        dictionary.forEach(bloomFilter::add);

        final Scanner scanner = new Scanner(System.in);
        BloomFilterUtils.log("Waiting for the input in order to check word existence in Bloom filter...");
        while (scanner.hasNext()) {
            final String word = scanner.next();
            if (word.equals("quit")) {
                break;
            }

            boolean contains = bloomFilter.contains(word);
            BloomFilterUtils.log("The word '%s' is checked. Bloom filter contains: %s", word, contains);
        }

    }


}
