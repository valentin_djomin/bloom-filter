import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BloomFilterTest {

    @Test
    void bloomFilter_contains() {
        BloomFilterUtils.log("Test bloom filter contains method functionality.");
        final List<String> dictionary = BloomFilterUtils.readFile(BloomFilterConstants.DICTIONARY_DEFAULT_PATH_UNIX);
        final BloomFilter bloomFilter = new BloomFilter(dictionary.size(), BloomFilterConstants.BIT_VECTOR_SIZE_1MB);
        dictionary.forEach(bloomFilter::add);

        assertFalse(bloomFilter.contains(UUID.randomUUID().toString()));
        assertFalse(bloomFilter.contains("ADEN"));
        assertFalse(bloomFilter.contains("ValentinD"));

        //Bloom filter should contain 50 random words taken from dictionary.
        final Random random = new Random();
        IntStream.range(0, 50).forEach(index -> {
            int randomIndex = random.nextInt() % dictionary.size();
            if (randomIndex < 0) randomIndex = -randomIndex;
            final String randomWordFromFile = dictionary.get(randomIndex);
            assertTrue(bloomFilter.contains(randomWordFromFile));
            BloomFilterUtils.log("Bloom filter contains '%s' word from dictionary.", randomWordFromFile);
        });
    }

    @Test
    void bloomFilter_insertionSpeed100_000Elements() {
        BloomFilterUtils.log("Test insertion speed of 100 000 elements to bit vector with 1MB size.");
        int elements = 100_000;
        final BloomFilter bloomFilter = new BloomFilter(elements, BloomFilterConstants.BIT_VECTOR_SIZE_1MB);
        long startTime = System.currentTimeMillis();
        IntStream.range(0, elements).forEach(bloomFilter::add);
        long endTime = System.currentTimeMillis();
        BloomFilterUtils.log("100_000 elements were inserted to the bloom filter within %s milliseconds.", endTime - startTime);
    }

    @Test
    void bloomFilter_insertionSpeed10_000_000Elements() {
        BloomFilterUtils.log("Test insertion speed of 10 000 000 elements to bit vector with 3MB size.");
        int elements = 10_000_000;
        final BloomFilter bloomFilter = new BloomFilter(elements, BloomFilterConstants.BIT_VECTOR_SIZE_3MB);
        long startTime = System.currentTimeMillis();
        IntStream.range(0, elements).forEach(bloomFilter::add);
        long endTime = System.currentTimeMillis();
        BloomFilterUtils.log("10_000_000 elements were inserted to the bloom filter within %s milliseconds.", endTime - startTime);
    }
}
