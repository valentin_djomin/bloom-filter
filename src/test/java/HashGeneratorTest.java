import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class HashGeneratorTest {

    @Test
    void getSetOfHashes_shouldGenerateSameHashesTwiceForSameObject() {
        final HashGenerator hashGenerator = new HashGenerator(25, BloomFilterConstants.BIT_VECTOR_SIZE_1MB);
        final String randomStr = UUID.randomUUID().toString();
        final Set<Integer> setOfHashes = hashGenerator.getSetOfHashes(randomStr);
        final Set<Integer> setOfHashes2 = hashGenerator.getSetOfHashes(randomStr);

        assertEquals(setOfHashes.size(), setOfHashes2.size());
        setOfHashes.forEach(hash -> assertTrue(setOfHashes2.contains(hash)));
    }
}
