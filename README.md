## Bloom filter description
A Bloom filter is a space-efficient probabilistic data structure, conceived by Burton Howard Bloom in 1970, that is used to test whether an element is a member of a set. 
False positive matches are possible, but false negatives are not – in other words, a query returns either "possibly in set" or "definitely not in set."
Elements can be added to the set, but not removed (though this can be addressed with the counting Bloom filter variant);
the more items added, the larger the probability of false positives.

## Tech stack
This application is a basic implementation of Bloom filter using Java 11 and Maven.
Application read dictionary from default unix dict: /usr/share/dict/words

## How to run application?
It is a command line application using maven build automation tool.

1)Could be run as through the IDE with BloomFilterMain.class main class.
2)Could be run as jar-file:
    Run maven install from project root folder to build project and generate jar-file:
    ```
    mvn clean install
    ```
    Run generated jar file:
    ```
    java -jar bloomfilter-1.0-SNAPSHOT.jar
    ```
